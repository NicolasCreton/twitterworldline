package worldline.ssm.rd.ux.wltwitter.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import worldline.ssm.rd.ux.wltwitter.R;

/**
 * Created by nicolas on 26/10/2015.
 */
public class EndFetchingTweetsReceiver extends BroadcastReceiver {
    View tweetsView;
    String subText;

    public EndFetchingTweetsReceiver() {
    }

    public EndFetchingTweetsReceiver(View tweetsView, String subText) {
        this.tweetsView = tweetsView;
        this.subText = subText;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        ProgressBar progressBar = (ProgressBar) tweetsView.findViewById(R.id.progressBar);
        Toolbar toolbar = (Toolbar) tweetsView.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.setSubtitle("@" + subText);
        progressBar.setVisibility(View.INVISIBLE);
    }
}
