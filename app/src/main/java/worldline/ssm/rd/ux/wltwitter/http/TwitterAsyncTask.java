package worldline.ssm.rd.ux.wltwitter.http;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.helpers.TwitterHelper;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

/**
 * Created by nicolas on 02/10/2015.
 */
public class TwitterAsyncTask extends AsyncTask<String, Integer, List<Tweet>>{

    TweetListener listener;
    public TwitterAsyncTask(TweetListener tweetListener){
        listener = tweetListener;
    }

    @Override
    protected List<Tweet> doInBackground(String... params) {
        String login = params[0].toString();
        if(login != null){
            List<Tweet> tweets = TwitterHelper.getTweetsOfUser(login);
            return tweets;
        }
        return null;
    }

    protected void onPostExecute(List<Tweet> tweets){
        listener.onTweetsRetrieved(tweets);
    }

    public interface TweetListener {
        public void onTweetsRetrieved (List<Tweet> tweets);
    }
}
