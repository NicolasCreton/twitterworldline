package worldline.ssm.rd.ux.wltwitter.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.Calendar;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.database.WLTwitterDatabaseManager;
import worldline.ssm.rd.ux.wltwitter.fragment.TweetFragment;
import worldline.ssm.rd.ux.wltwitter.fragment.TweetsFragment;
import worldline.ssm.rd.ux.wltwitter.interfaces.ListenerInterfaces;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;
import worldline.ssm.rd.ux.wltwitter.receivers.NewTweetsReceiver;
import worldline.ssm.rd.ux.wltwitter.service.WLTwitterService;
import worldline.ssm.rd.ux.wltwitter.utils.Constants;
import worldline.ssm.rd.ux.wltwitter.utils.NotificationsUtils;


public class WLTwitterActivity extends AppCompatActivity implements ListenerInterfaces.tweetClickedListener, ListenerInterfaces.onRTButtonClickedListener {


    private PendingIntent mServicePendingIntent;
    private NewTweetsReceiver tweetsReceiver;

    @Override
    protected void onResume() {
        super.onResume();
        final Calendar calendar = Calendar.getInstance();
        final Intent serviceIntent = new Intent(this, WLTwitterService.class);
        mServicePendingIntent = PendingIntent.getService(this, 0, serviceIntent, 0);
        final AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 60000, mServicePendingIntent);

        tweetsReceiver = new NewTweetsReceiver();
        registerReceiver(tweetsReceiver, new IntentFilter(Constants.General.ACTION_NEW_TWEETS));

        Bundle extras = new Bundle();
        extras.putString("loginFile", getIntent().getExtras().getString("login"));
        serviceIntent.putExtras(extras);
        startService(serviceIntent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        final AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(mServicePendingIntent);
        //Toast.makeText(this, "Service stopped", Toast.LENGTH_SHORT).show();
        Log.d("Service", "Stopped");
        unregisterReceiver(tweetsReceiver);
        tweetsReceiver = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WLTwitterDatabaseManager.dropDatabase();
        setContentView(R.layout.twitter_activity);
        transaction();
    }

    public void transaction() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        TweetsFragment fragment = new TweetsFragment();
        transaction.add(R.id.rootLayout, fragment);
        transaction.commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.wltwitter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.actionLogout) {
            logout();
        }
        if (id == R.id.actionSettings) {
            Intent intent = new Intent(this, WLTwitterSettings.class);
            startActivity(intent);
        }
        if (id == R.id.actionAbout) {
            Intent intent = new Intent(this, WLTwitterAbout.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    public void logout() {
        SharedPreferences loginSharedPreferences = getSharedPreferences("loginFile", Context.MODE_PRIVATE);
        loginSharedPreferences.edit().clear().commit();
        finish();
    }

    @Override
    public void onTweetClicked(Tweet tweet) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        TweetFragment fragment = new TweetFragment();
        final Bundle bundleTweet = new Bundle();
        bundleTweet.putParcelable("tweet", tweet);
        fragment.setArguments(bundleTweet);
        transaction.replace(R.id.rootLayout, fragment);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.addToBackStack(null);
        transaction.commit();
        hapticFeedback();
    }

    public void hapticFeedback() {
        SharedPreferences settingsSharedPreferences = getSharedPreferences("twitter_settings_file", Context.MODE_PRIVATE);
        if (settingsSharedPreferences.getBoolean("hapticFeedback", true) == true) {
            View tweetsView = findViewById(R.id.tweetsListView);
            tweetsView.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        }
    }

    public void activateHapticFeedBack() {
        SharedPreferences settingsSharedPreferences = getSharedPreferences("twitter_settings_file", Context.MODE_PRIVATE);
        settingsSharedPreferences.edit().putBoolean("hapticFeedback", true).commit();
    }


    @Override
    public void onRTButtonClicked(Tweet tweet) {
        Snackbar.make(findViewById(R.id.tweets_coord_layout), "RT : " + tweet.text, Snackbar.LENGTH_SHORT).show();
        NotificationsUtils.retweetNotification(tweet, true, true);
    }
}

