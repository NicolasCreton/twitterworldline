package worldline.ssm.rd.ux.wltwitter.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import worldline.ssm.rd.ux.wltwitter.R;

/**
 * Created by nicolas on 25/09/2015.
 */
public class WLTwitterLoginActivity extends Activity implements View.OnClickListener{

    private Boolean stayLoggedBool = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        String defaultValue = getResources().getString(R.string.defaultValue);

        SharedPreferences sharedPreferences = getSharedPreferences("loginFile", Context.MODE_PRIVATE);
        final String login = sharedPreferences.getString("login", defaultValue);
        if (!login.equals(defaultValue) && sharedPreferences.getBoolean("stayLoggedIn", true)) {
            loggingIn(login);
        }
        setContentView(R.layout.activity_main);
        findViewById(R.id.button).setOnClickListener(this);
        Switch logSwitch = (Switch) findViewById(R.id.switchStayLoggedIn);
        logSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                stayLoggedBool = isChecked;
            }
        });
    }

    @Override
    public void onClick(View v) {

        EditText loginText = (EditText)findViewById(R.id.nameBox);
        EditText passwordText = (EditText)findViewById(R.id.passwordBox);

        if (TextUtils.isEmpty(loginText.getText())){
            Toast.makeText(this, R.string.error_no_login, Toast.LENGTH_LONG).show();
        }else  if(TextUtils.isEmpty(passwordText.getText())){
            Toast.makeText(this, R.string.error_no_password, Toast.LENGTH_LONG).show();
        }else {
            loggingIn(loginText.getText().toString());
        }
    }

    public void loggingIn(String loginText) {
        Intent intent = new Intent(this, WLTwitterActivity.class);
        Bundle extras = new Bundle();
        extras.putString("login", loginText);
        intent.putExtras(extras);
        SharedPreferences sharedPreferences = getSharedPreferences("loginFile", Context.MODE_PRIVATE);
        sharedPreferences.edit().putString("login", loginText).commit();
        sharedPreferences.edit().putBoolean("stayLoggedIn", stayLoggedBool).commit();
        startActivity(intent);
    }
}
