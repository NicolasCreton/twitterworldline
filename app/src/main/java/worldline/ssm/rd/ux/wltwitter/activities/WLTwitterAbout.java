package worldline.ssm.rd.ux.wltwitter.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;

import worldline.ssm.rd.ux.wltwitter.R;

/**
 * Created by Nicolas Creton on 28/09/2015.
 */
public class WLTwitterAbout extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.twitter_about);
        //getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
