package worldline.ssm.rd.ux.wltwitter.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.activities.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by nicolas on 09/10/2015.
 */
public class TweetFragment extends android.support.v4.app.Fragment {
    public void setArgument() {

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.tweet_fragment_layout, container, false);
        final Tweet tweet = getArguments().getParcelable("tweet");
        TextView contentText = (TextView) rootView.findViewById(R.id.content);
        contentText.setText(tweet.text);
        TextView screenNameText = (TextView) rootView.findViewById(R.id.userAlias);
        screenNameText.setText("@" + tweet.user.screenName);

        ImageView profilePic = (ImageView) rootView.findViewById(R.id.picture);

        Picasso.with(getActivity().getApplicationContext())
                .load(tweet.user.profileImageUrl)
                .placeholder(R.drawable.downloading_icon)
                .resize(100, 100)
                .centerCrop()
                .into(profilePic);

        TextView nameText = (TextView) rootView.findViewById(R.id.name);
        nameText.setText(tweet.user.name);

        View rtButton = rootView.findViewById(R.id.retweet_item);
        settingListenerOnClick(rtButton, "Retweet");
        View replyButton = rootView.findViewById(R.id.reply_item);
        settingListenerOnClick(replyButton, "Reply");
        View starButton = rootView.findViewById(R.id.star_item);
        settingListenerOnClick(starButton, "Star");

        return rootView;
    }

    public void settingListenerOnClick(View button, final String message) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(WLTwitterApplication.getContext(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
}
