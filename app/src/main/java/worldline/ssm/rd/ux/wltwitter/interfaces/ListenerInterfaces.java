package worldline.ssm.rd.ux.wltwitter.interfaces;

import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by Nicolas on 13/10/2015.
 */
public class ListenerInterfaces {

    public interface tweetClickedListener {
        void onTweetClicked(Tweet tweet);
    }

    public interface onRTButtonClickedListener {
        void onRTButtonClicked(Tweet tweet);
    }
}
