package worldline.ssm.rd.ux.wltwitter.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.activities.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.database.WLTwitterDatabaseManager;
import worldline.ssm.rd.ux.wltwitter.holder.ViewHolder;
import worldline.ssm.rd.ux.wltwitter.interfaces.ListenerInterfaces;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by nicolas on 23/10/2015.
 */
public class TweetsCursorAdapter extends CursorRecyclerViewAdapter<ViewHolder> {

    private List<Tweet> tweetList;
    private ListenerInterfaces.tweetClickedListener clickedListener;

    public TweetsCursorAdapter(Context context, Cursor cursor, ListenerInterfaces.tweetClickedListener click) {
        super(context, cursor);
        clickedListener = click;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Cursor cursor) {
        final Tweet tweet = WLTwitterDatabaseManager.tweetFromCursor(cursor);
        viewHolder.setListeners(tweet, clickedListener);

        viewHolder.name.setText(tweet.user.name);
        viewHolder.alias.setText("@" + tweet.user.screenName);
        viewHolder.text.setText(tweet.text);
        Picasso.with(WLTwitterApplication.getContext())
                .load(tweet.user.profileImageUrl)
                .placeholder(R.drawable.downloading_icon)
                .resize(100, 100)
                .centerCrop()
                .into(viewHolder.image);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(WLTwitterApplication.getContext());
        View view = inflater.inflate(R.layout.tweet_layout_item, null);
        return new ViewHolder(view);
    }
}
