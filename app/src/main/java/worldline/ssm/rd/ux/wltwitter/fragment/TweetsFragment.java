package worldline.ssm.rd.ux.wltwitter.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.activities.WLTwitterActivity;
import worldline.ssm.rd.ux.wltwitter.activities.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.adapter.TweetsCursorAdapter;
import worldline.ssm.rd.ux.wltwitter.database.WLTwitterDatabaseContract;
import worldline.ssm.rd.ux.wltwitter.receivers.EndFetchingTweetsReceiver;
import worldline.ssm.rd.ux.wltwitter.receivers.FetchingTweetsReceiver;
import worldline.ssm.rd.ux.wltwitter.utils.Constants;


/**
 * Created by nicolas on 02/10/2015.
 */
public class TweetsFragment extends android.support.v4.app.Fragment implements android.support.v4.app.LoaderManager.LoaderCallbacks<Cursor> {

    RecyclerView mListView;
    String login;
    Toolbar tbar;
    View rootView;
    FetchingTweetsReceiver fetchnigTweetsReceiver;
    EndFetchingTweetsReceiver endFetchingTweetsReceiver;

    @Override
    public void onStart() {
        super.onStart();
        getLoaderManager().initLoader(0, null, this);
        login = getLogin();
        tbar.setSubtitle("@" + login);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(fetchnigTweetsReceiver);
        getActivity().unregisterReceiver(endFetchingTweetsReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        setReceivers();
    }

    public String getLogin() {
        String defaultValue = getResources().getString(R.string.defaultValue);
        SharedPreferences sharedPreferences = WLTwitterApplication.getContext().getSharedPreferences("loginFile", Context.MODE_PRIVATE);
        String login = sharedPreferences.getString("login", defaultValue);
        return login;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.tweets_fragment_layout, container, false);
        tbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(tbar);
        mListView = (RecyclerView) rootView.findViewById(R.id.tweetsListView);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(WLTwitterApplication.getContext());
        this.mListView.setLayoutManager(layoutManager);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.wltwitter, menu);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }


    @Override
    public android.support.v4.content.Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        final android.support.v4.content.CursorLoader cursorLoader = new android.support.v4.content.CursorLoader(WLTwitterApplication.getContext());
        cursorLoader.setUri(WLTwitterDatabaseContract.TWEETS_URI);
        cursorLoader.setProjection(WLTwitterDatabaseContract.PROJECTION_FULL);
        cursorLoader.setSelection(null);
        cursorLoader.setSelectionArgs(null);
        cursorLoader.setSortOrder(null);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader, Cursor data) {
        final TweetsCursorAdapter tweetsCursorAdapter = new TweetsCursorAdapter(getActivity(), data, (WLTwitterActivity) getActivity());
        mListView.setAdapter(tweetsCursorAdapter);
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<Cursor> loader) {

    }

    public void setReceivers() {
        fetchnigTweetsReceiver = new FetchingTweetsReceiver(rootView, login);
        getActivity().registerReceiver(fetchnigTweetsReceiver, new IntentFilter(Constants.General.START_FETCHING_INTENT));
        endFetchingTweetsReceiver = new EndFetchingTweetsReceiver(rootView, login);
        getActivity().registerReceiver(endFetchingTweetsReceiver, new IntentFilter(Constants.General.END_FETCHING_INTENT));
    }
}
