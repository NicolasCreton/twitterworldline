package worldline.ssm.rd.ux.wltwitter.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import java.util.List;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.activities.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.database.WLTwitterDatabaseManager;
import worldline.ssm.rd.ux.wltwitter.http.TwitterAsyncTask;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;
import worldline.ssm.rd.ux.wltwitter.utils.Constants;

/**
 * Created by nicolas on 23/10/2015.
 */
public class WLTwitterService extends Service implements TwitterAsyncTask.TweetListener {


    Intent startFetchingIntent;
    Intent endFetchingIntent;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Toast.makeText(this, "Service Starting", Toast.LENGTH_SHORT).show();
        Log.d("Service", "Starting");
        String defaultValue = getResources().getString(R.string.defaultValue);
        SharedPreferences sharedPreferences = WLTwitterApplication.getContext().getSharedPreferences("loginFile", Context.MODE_PRIVATE);
        String login = sharedPreferences.getString("login", defaultValue);
        if (!TextUtils.isEmpty(login)) {
            new TwitterAsyncTask(this).execute(login);
        }

        startFetchingIntent = new Intent(Constants.General.START_FETCHING_INTENT);
        sendBroadcast(startFetchingIntent);

        return Service.START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onTweetsRetrieved(List<Tweet> tweets) {
        int nbTweetReceived = 0;

        for (Tweet tweet : tweets) {
            final int id = WLTwitterDatabaseManager.insertTweet(tweet);
            if (id > -1) {
                nbTweetReceived++;
            }
        }

        if (nbTweetReceived > 0) {
            final Intent newTweetsIntent = new Intent(Constants.General.ACTION_NEW_TWEETS);
            final Bundle extras = new Bundle();
            extras.putInt(Constants.General.ACTION_NEW_TWEETS_EXTRA_NB_TWEETS, nbTweetReceived);
            newTweetsIntent.putExtras(extras);
            sendBroadcast(newTweetsIntent);
        }

        endFetchingIntent = new Intent(Constants.General.END_FETCHING_INTENT);
        sendBroadcast(endFetchingIntent);

        stopSelf();
    }
}
