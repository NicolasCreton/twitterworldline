package worldline.ssm.rd.ux.wltwitter.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.activities.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.activities.WLTwitterLoginActivity;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by Streyan on 10/26/2015.
 */
public class NotificationsUtils {


    public static void displayNewTweetsNotification(int nbTweets, boolean vibrate, boolean playsound) {
        final Context context = WLTwitterApplication.getContext();

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setColor(42)
                .setSmallIcon(R.drawable.ic_twitter)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(String.format(context.getString(R.string.notification_newTweets) + " " + nbTweets))
                .setAutoCancel(true);

        final Intent newIntent = new Intent(context, WLTwitterLoginActivity.class);
        newIntent.setFlags((Intent.FLAG_ACTIVITY_NEW_TASK));

        final TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(WLTwitterLoginActivity.class);
        stackBuilder.addNextIntent(newIntent);
        final PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        final Notification notification = mBuilder.build();
        notification.category = Notification.CATEGORY_SOCIAL;

        if (vibrate) {
            notification.defaults = Notification.DEFAULT_VIBRATE;
        }

        if (playsound) {
            notification.sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        }

        final NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(42, notification);
    }

    public static void retweetNotification(Tweet textTweet, boolean vibrate, boolean playsound) {
        final Context context = WLTwitterApplication.getContext();

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setColor(42)
                .setSmallIcon(R.drawable.ic_retweet)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(String.format(context.getString(R.string.notifaction_retweet) + " " + textTweet.text))
                .setAutoCancel(true);

        final Notification notification = mBuilder.build();
        notification.category = Notification.CATEGORY_SOCIAL;

        if (vibrate) {
            notification.defaults = Notification.DEFAULT_VIBRATE;
        }

        if (playsound) {
            notification.sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        }

        final NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(42, notification);
    }
}
