package worldline.ssm.rd.ux.wltwitter.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.activities.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.holder.ViewHolder;
import worldline.ssm.rd.ux.wltwitter.interfaces.ListenerInterfaces;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by nicolas on 09/10/2015.
 */
public class TweetsAdapter extends RecyclerView.Adapter<ViewHolder> {

    private List<Tweet> tweetList;
    private ListenerInterfaces.tweetClickedListener clickedListener;

    public TweetsAdapter(List<Tweet> tweets, ListenerInterfaces.tweetClickedListener click) {
        tweetList = tweets;
        this.clickedListener = click;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(WLTwitterApplication.getContext());
        View view = inflater.inflate(R.layout.tweet_layout_item, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final Tweet tweet = tweetList.get(position);
        viewHolder.setListeners(tweet, clickedListener);

        viewHolder.name.setText(tweet.user.name);
        viewHolder.alias.setText("@" + tweet.user.screenName);
        viewHolder.text.setText(tweet.text);
        Picasso.with(WLTwitterApplication.getContext())
                .load(tweet.user.profileImageUrl)
                .placeholder(R.drawable.downloading_icon)
                .resize(100, 100)
                .centerCrop()
                .into(viewHolder.image);
    }

    @Override
    public int getItemCount() {
        return tweetList.size();
    }

}
