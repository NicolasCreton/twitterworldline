package worldline.ssm.rd.ux.wltwitter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.interfaces.ListenerInterfaces;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by nicolas on 09/10/2015.
 */
public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView image;
    public TextView name;
    public TextView alias;
    public TextView text;
    public ImageButton button;

    private View view;
    private Tweet tweet;
    private ListenerInterfaces.tweetClickedListener listener;



    public ViewHolder(View view) {
        super(view);
        this.view = view;
        view.setClickable(true);
        view.setOnClickListener(this);
        image = (ImageView) view.findViewById(R.id.profilePicture);
        name = (TextView) view.findViewById(R.id.userName);
        alias = (TextView) view.findViewById(R.id.alias);
        text = (TextView) view.findViewById(R.id.tweetContent);
        button = (ImageButton) view.findViewById(R.id.rtButton);
        button.setOnClickListener(this);
    }

    public void setListeners(Tweet tweet, ListenerInterfaces.tweetClickedListener listener) {
        this.tweet = tweet;
        this.listener = listener;
        view.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v instanceof ImageButton) {
            ListenerInterfaces.onRTButtonClickedListener buttonRT = (ListenerInterfaces.onRTButtonClickedListener) listener;
            buttonRT.onRTButtonClicked(tweet);
        } else {
            listener.onTweetClicked(tweet);
        }
    }

}
