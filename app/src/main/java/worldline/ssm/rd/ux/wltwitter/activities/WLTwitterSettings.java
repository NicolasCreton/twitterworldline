package worldline.ssm.rd.ux.wltwitter.activities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import worldline.ssm.rd.ux.wltwitter.R;

/**
 * Created by nicolas on 27/09/2015.
 */
public class WLTwitterSettings extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.twitter_settings);
        //getActionBar().setDisplayHomeAsUpEnabled(true);
        final Switch hapticSwitch = (Switch) findViewById(R.id.feedbackSwitch);
        restoreSwitchState(hapticSwitch, "hapticFeedback");
        setListenerToSwitch(hapticSwitch, "hapticFeedback", "Haptic feedback is");
    }

    public void restoreSwitchState(Switch aSwitch, String keyName) {
        aSwitch.setChecked(true);
        SharedPreferences sharedPreferences = getSharedPreferences("twitter_settings_file", Context.MODE_PRIVATE);
        if (sharedPreferences.getBoolean(keyName, true) == true) {
            aSwitch.setChecked(true);
        } else {
            aSwitch.setChecked(false);
        }
    }

    public void setListenerToSwitch(final Switch listenerToSwitch, final String keyName, final String toastText) {
        listenerToSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences sharedPreferences = getSharedPreferences("twitter_settings_file", Context.MODE_PRIVATE);
                if (listenerToSwitch.isChecked() == false) {
                    sharedPreferences.edit().putBoolean(keyName, false).commit();
                    Toast.makeText(WLTwitterSettings.this, toastText + " OFF", Toast.LENGTH_SHORT).show();
                } else {
                    sharedPreferences.edit().putBoolean(keyName, true).commit();
                    Toast.makeText(WLTwitterSettings.this, toastText + " ON", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }


}
