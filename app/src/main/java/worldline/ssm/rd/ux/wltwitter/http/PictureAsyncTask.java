package worldline.ssm.rd.ux.wltwitter.http;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

import worldline.ssm.rd.ux.wltwitter.helpers.TwitterHelper;

/**
 * Created by Streyan on 10/11/2015.
 */
public class PictureAsyncTask extends AsyncTask<String, String , Bitmap> {

    public ImageView imageView;

    public PictureAsyncTask(ImageView image){
        imageView = image;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        try {
            return TwitterHelper.getTwitterUserImage(params[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if(imageView != null){
            imageView.setImageBitmap(bitmap);
        }
    }
}
